module quad(
    input clk,
    input channelA,
    input channelB,
    output reg [7:0] count
);

    reg [2:0] channelA_delayed, channelB_delayed;
    always @(posedge clk) begin
        channelA_delayed <= {channelA_delayed[1:0], channelA};
        channelB_delayed <= {channelB_delayed[1:0], channelB};
    end

    wire count_enable = channelA_delayed[1] ^ channelA_delayed[2] ^ channelB_delayed[1] ^ channelB_delayed[2];
    wire count_direction = channelA_delayed[1] ^ channelB_delayed[2];

    always @(posedge clk) begin
        if(count_enable) begin
            if(count_direction) begin
                count <= count + 1;
            end else begin
                count <= count - 1;
            end
        end
    end

endmodule
