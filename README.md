# FPGA

Using system image provided by RocketBoards.org

- [Board Configuration](https://rocketboards.org/foswiki/Documentation/SoCSWWorkshopSeriesBoardConfiguration#A_42Atlas_47_DE0_NANO_SOC_Board_Configuration_42)
- [System Images](https://rocketboards.org/foswiki/Documentation/SoCSWWorkshopSeriesSDCardImages)

## TODO:

- automatically build .rbf and .dtb (binary) files
    - dtc, quartus_cpf, make